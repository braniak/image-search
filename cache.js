var constants = require('./constants');

var requests = [];

exports.addRequest = function(request) {
    if (requests.length >= constants.REQUESTS_COUNT) {
        requests.pop();
    }
    requests.push({request: request, date: new Date()});
};

exports.getRequests = function() {
    return requests;
};