var constants = {
    DEFAULT_URL: 'api.cognitive.microsoft.com',
    DEFAULT_PATH: '/bing/v5.0/images/search',
    KEY: '07d1ec5b5645413aa0b352c08ef3e15a',
    COUNT: 10,
    REQUESTS_COUNT: 10
};

module.exports = exports = constants;