var https = require('https');
var constants = require('./constants');

exports.search = function(searchStr, offset, callback) {
    var path = constants.DEFAULT_PATH + '?q=' + searchStr + '&count=' + constants.COUNT;
    if (offset) {
        path += '&offset=' + offset;
    }
    var options = {
        hostname: constants.DEFAULT_URL,
        path: path,
        headers: {'Ocp-Apim-Subscription-Key': constants.KEY}
    };

    var req = https.get(options, function(res) {

        var code = res.statusCode;
        if (code === 200) {
            var data = '';

            res.on('data', function(chunk) {
                data += chunk;
            });
            res.on('end', function() {
                var images = [];
                var rawImages = JSON.parse(data).value;
                rawImages.forEach(function(rawImage) {
                    images.push({
                        url: rawImage.contentUrl,
                        name: rawImage.name,
                        thumbnailUrl: rawImage.thumbnailUrl,
                        hostPageUrl: rawImage.hostPageUrl
                    })
                });
                callback(null, images);
            });
        } else {
            callback({statusCode: code, message: res.statusMessage});
        }
    });

    req.on('error', function(err) {
        callback({error: err.message});
    });

};