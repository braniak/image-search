var http = require('http');
var url = require('url');
var querystring = require('querystring');
var constants = require('./constants');
var imageSearch = require('./image-search');
var cache = require('./cache');

var html = '<h1>Image search microservice</h1>' +
    '<p>Use <code>https://image-search26.herokuapp.com/imagesearch/&lt;request&gt;</code> to search for images</p>' +
    '<p>Use <code>https://image-search26.herokuapp.com/latest</code> to view most current searches</p>';

var server = http.createServer(function(req, res) {

    var urlObj = url.parse(req.url, true);

    var path = urlObj.pathname.split('/')[1];
    switch (path) {
        case 'imagesearch': {
            var offset = urlObj.query && urlObj.query.offset;
            var searchStr = querystring.unescape(urlObj.pathname.split('/')[2]);

            if (searchStr) {
                cache.addRequest(searchStr);
                res.setHeader('Content-type', 'application/json');

                imageSearch.search(searchStr, offset, function(err, images) {
                    if (err) {
                        res.end(JSON.stringify(err));
                    }
                    res.end(JSON.stringify(images));
                });
            } else {
                res.setHeader('Content-type', 'text/html');
                res.end(html)
            }
            break;
        }
        case 'latest': {
            res.setHeader('Content-type', 'application/json');
            res.end(JSON.stringify(cache.getRequests()));
            break;
        }
        default:
            res.setHeader('Content-type', 'text/html');
            res.end(html)
    }


});

server.listen(process.env.PORT || 3000);
